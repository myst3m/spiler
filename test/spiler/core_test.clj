(ns spiler.core-test
  (:require [clojure.test :refer :all]
            [spiler.core :refer :all]
            [spiler.bnf :as bnf]))

(deftest bnf-literal-test
  (testing "literal"
    (is (= (bnf/parser "1") [:S [:decimal "1"]]))
    (is (= (bnf/parser "123") [:S [:decimal "123"]]))
    (is (= (bnf/parser ":a")) [:S [:keyword "a"]])
    (is (= (bnf/parser "'a")) [:S [:symbol "'a"]])
    (is (= (bnf/parser "[ 1 2 3]")) [:S [:vector [:decimal "1"] [:decimal "2"] [:decimal "3"]]])
    (is (bnf/parser "[1 2 ^ints 10]") [:S
                                       [:vector
                                        [:decimal "1"]
                                        [:decimal "2"]
                                        [:with-meta [:type-hint [:symbol "ints"]] [:symbol "10"]]]])
    (is (= (bnf/parser "#{10 20 30}") [:S [:set [:decimal "10"] [:decimal "20"] [:decimal "30"]]]))
    (is (= (bnf/parser "{:a 10 :b 20}") [:S [:map [:keyword "a"] [:decimal "10"] [:keyword "b"] [:decimal "20"]]]))
    (is (= (bnf/parser "'(1 2 3)")) [:S [:quote-form [:list [:decimal "1"] [:decimal "2"] [:decimal "3"]]]])
    (is (bnf/parser "'(1 2 '[10 20 'a])") [:S
                                           [:quote-form
                                            [:list
                                             [:decimal "1"]
                                             [:decimal "2"]
                                             [:quote-form
                                              [:vector
                                               [:decimal "10"]
                                               [:decimal "20"]
                                               [:quote-form [:symbol "a"]]]]]]])))

(deftest bnf-form-test
  (testing "defn"
    (is (= (bnf/parser "(defn a [] 10)") [:S [:defn-form [:symbol "a"] [:fn-args] [:body [:decimal "10"]]]])))
  (testing "fn"
    (is (= (bnf/parser "(fn [a b c] (+ c a b))") [:S
                                                  [:fn-form
                                                   [:fn-args
                                                    [:destructuring-symbol [:symbol "a"]]
                                                    [:destructuring-symbol [:symbol "b"]]
                                                    [:destructuring-symbol [:symbol "c"]]]
                                                   [:body
                                                    [:list [:symbol "+"] [:symbol "c"] [:symbol "a"] [:symbol "b"]]]]] ))
    (is (= (bnf/parser "(fn [[a d] b c] (+ c a b))") [:S
                                                      [:fn-form
                                                       [:fn-args
                                                        [:destructuring-vector [:symbol "a"] [:symbol "d"]]
                                                        [:destructuring-symbol [:symbol "b"]]
                                                        [:destructuring-symbol [:symbol "c"]]]
                                                       [:body
                                                        [:list [:symbol "+"] [:symbol "c"] [:symbol "a"] [:symbol "b"]]]]]))
    (is (= (bnf/parser "(fn [{:keys[a d] :as e :or {a 100}} b c] (+ c a b e))")
           [:S
            [:fn-form
             [:fn-args
              [:destructuring-map
               [:keyword "keys"]
               [:vector [:symbol "a"] [:symbol "d"]]
               [:destructuring-element-as [:symbol "e"]]
               [:destructuring-element-or
                [:map [:symbol "a"] [:decimal "100"]]]]
              [:destructuring-symbol [:symbol "b"]]
              [:destructuring-symbol [:symbol "c"]]]
             [:body
              [:list
               [:symbol "+"]
               [:symbol "c"]
               [:symbol "a"]
               [:symbol "b"]
               [:symbol "e"]]]]])))
  (testing "deref"
    (is (= (bnf/parser "@a") [:S [:deref-form [:symbol "a"]]]))
    (is (= (bnf/parser "@(:a b)") [:S [:deref-form [:list [:keyword "a"] [:symbol "b"]]]])))
  (testing "syntax-quote"
    (is (= (bnf/parser "`(let [a# ~b] ~@body)") [:S
                                                 [:syntax-quote-form
                                                  [:let-form
                                                   [:binding
                                                    [:destructuring-symbol [:symbol "a#"]]
                                                    [:unquote-form [:symbol "b"]]]
                                                   [:body [:unquote-splicing-form [:symbol "body"]]]]]]))
    )
  (testing "var-quote"
    (is (= (bnf/parser "#'a") [:S [:var-quote-form [:symbol "a"]]])))
  (testing "def"
    (is (= (bnf/parser "(def a 100)") [:S [:def-form [:symbol "a"] [:decimal "100"]]]))
    (is (= (bnf/parser "(def a (atom {}))") [:S [:def-form [:symbol "a"] [:list [:symbol "atom"] [:map]]]])))
  (testing "let"
    (is (= (bnf/parser "(let [a 10] a)") [:S
                                          [:let-form
                                           [:binding [:destructuring-symbol [:symbol "a"]] [:decimal "10"]]
                                           [:body [:symbol "a"]]]]))
    (is (= (bnf/parser "(let [a ^long 1000 {:keys [b c]} [100 200]] a)") [:S
                                                                          [:let-form
                                                                           [:binding
                                                                            [:destructuring-symbol [:symbol "a"]]
                                                                            [:with-meta [:type-hint [:symbol "long"]] [:decimal "1000"]]]
                                                                           [:binding
                                                                            [:destructuring-map
                                                                             [:keyword "keys"]
                                                                             [:vector [:symbol "b"] [:symbol "c"]]]
                                                                            [:vector [:decimal "100"] [:decimal "200"]]]
                                                                           [:body [:symbol "a"]]]])))
  (testing "if"
    (is (= (bnf/parser "(if a b c)") [:S [:if-form [:symbol "a"] [:symbol "b"] [:symbol "c"]]])))
  (testing "when"
    (is (= (bnf/parser "(when a b c d)") [:S
                                          [:when-form [:symbol "a"] [:symbol "b"] [:symbol "c"] [:symbol "d"]]])))
  (testing "lambda"
    (is (= (bnf/parser "#(+ 10 20 %)") [:S
                                 [:lambda-form
                                  [:symbol "+"]
                                  [:decimal "10"]
                                  [:decimal "20"]
                                  [:symbol "%"]]])))
)
