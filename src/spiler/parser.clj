(ns spiler.parser
  (:gen-class)
  (:require [clojure.string :as str]
            [instaparse.core :as i]
            [silvur.log :as log]
            [spiler.bnf :as bnf]
            [spiler.core :refer :all]
            [silvur.datetime :refer :all]
            [clojure.tools.cli :refer [parse-opts]]
            [clojure.java.io :as io]
            [clojure.pprint :refer (pprint)]))

(def built-in-macros
  [{:ns 'clojure.core
    :symbol 'lambda
    :macro? true
    :fn (fn [& xs]
          (let [counter (volatile! [])
                form (clojure.walk/postwalk
                      (fn [z]
                        (if (and (string? z) (re-find #"[%]" z))
                          (let [v (str (gensym "La_"))]
                            (vswap! counter conj v)
                            v)
                          z))
                      xs)]
            [:fn-form (apply vector
                             :fn-args
                             (map (fn [z]
                                    [:destructuring-symbol [:symbol z]])
                                  @counter))
             [:body (apply vector :list form)]]))}
   {:ns 'clojure.core
    :symbol 'when
    :mamcro? true
    :fn (fn [condition & body]
          [:if-form
           condition
           [:do-form (apply vector :body body)]
           [:nil "nil"]])}
   {:ns 'clojure.core
    :symbol 'doto
    :macro? true
    :fn (fn [x & xs]
          (let [r-temp [:symbol (str (gensym "M_"))]]
            (vector :let-form
                    [:binding [:destructuring-symbol r-temp] x ]
                    (apply vector :body (conj (vec (map (fn [z]
                                                          (let [[tag element & r] z]
                                                            (if (= tag :list)
                                                              (apply vector :list element r-temp r)
                                                              [:list z r-temp])))
                                                        xs))
                                              r-temp)))))}
   {:ns 'clojure.core
    :symbol 'defn
    :macro? true
    :fn (fn [sym & xs]
          (let [body (last xs)
                args (last (drop-last xs))
                desc (last (drop-last 2 xs))]
            [:do-form [:body [:comment ";" (str " " (str/join (rest desc)))]
                       [:def-form sym [:fn-form args body]]]]))}
   {:ns 'clojure.core
    :symbol '->
    :macro? true
    :fn (fn [x & xs]
          (reduce (fn [r [tag & zs]]
                    (if (= tag :list)
                      (apply vector :list (first zs) r (rest zs))
                      [:list (first zs) r]))
                  x
                  xs))}
   {:ns 'clojure.core
    :symbol '->>
    :macro? true
    :fn (fn [x & xs]
          (reduce (fn [r [tag & zs]]
                    (if (= tag :list)
                      (apply vector :list (first zs) (conj (vec (rest zs)) r))
                      [:list (first zs) r]))
                  x
                  xs))}
   ])



(defn expand-builtin-macro [bnf-vec]
  (i/transform {:S (fn [& xs] (apply vector :S xs))
                :lambda-form (:fn (first (lookup* 'lambda built-in-macros )))
                :when-form (:fn (first (lookup* 'when built-in-macros )))
                :doto-form (:fn (first (lookup* 'doto built-in-macros )))
                :defn-form (:fn (first (lookup* 'defn built-in-macros )))
                :arrow-1-form (:fn (first (lookup* '-> built-in-macros )))
                :arrow-2-form (:fn (first (lookup* '->> built-in-macros )))}
               bnf-vec))

(defn create-node-tree [tree]
  (i/transform {:S (fn [& xs]
                     {:tag :S
                      :args xs})
                :list (fn [& xs]
                        (loop [result []
                               stack []
                               xs xs]
                          (if-not (seq xs)
                            {:tag :list
                             :args result}
                            (if (= :meta (:tag (first xs)))
                              (recur result (conj stack (first xs)) (rest xs))
                              (if (seq stack)
                                (recur (conj result (assoc (first xs) :meta (mapcat :args stack)))
                                       []
                                       (rest xs))
                                (recur (conj result (first xs)) stack (rest xs)))))))
                

                :boolean (fn [x]
                           {:tag :boolean
                            :args (list (symbol x))})
                
                :symbol (fn [x] {:tag :symbol
                                 :args (list (symbol x))})

                :meta-form (fn [x]
                             {:tag :meta
                              :args (list x)})

                :method (fn [x] {:tag :method
                                 :args (list (symbol x))})                

                :map (fn [& xs] {:tag :map  :args xs})
                
                :keyword (fn [& xs]
                           {:tag :keyword
                            :args xs})

                :ns-require (fn [& xs]
                              {:tag :ns-require
                               :args xs})

                :ns-import (fn [& xs]
                             {:tag :ns-import
                              :args xs})


                :quote-form (fn [& xs]
                              
                              {:tag :quote
                               :args (clojure.walk/postwalk-replace
                                      {:list :quote-list
                                       :symbol :string
                                       :number :string} xs)})
                :ns-form (fn [& xs]
                           {:tag :ns
                            :args xs})

                :ns-vector (fn [& xs]
                             {:tag :ns-vector
                              :args xs})

                

                :destructuring-map (fn [& xs]
                                     {:tag :destructuring-map
                                      :args xs})
                :destructuring-symbol (fn [& xs]
                                        {:tag :destructuring-symbol
                                         :args xs})
                :destructuring-vector (fn [& xs]
                                        {:tag :destructuring-vector
                                         :args xs})

                :set (fn [& xs] {:tag :set :args xs})
                :vector (fn [& xs] {:tag :vector :args xs})
                :binding (fn [& xs]
                           {:tag :binding
                            :args xs})
                :body (fn [& xs]
                        {:tag :body
                         :args xs})
                :fn-form (fn [& xs] {:tag :fn :args (:args  (first xs)) :body (last xs)})

                :fn-args (fn [& xs] {:tag :fn-args :args xs})
                :let-form (fn [& xs] {:tag :let :bindings (butlast xs) :body (last xs)})
                :if-form (fn [& xs] {:tag :if :condition (first xs) :t-body (second xs) :f-body (last xs)})
                :do-form (fn [& xs] {:tag :let :bindings '() :body (last xs)})
                :def-form (fn [& xs] {:tag :def :args xs})
                
                :decimal (fn [x] {:tag :number :args (list (Long/parseLong x))})
                :nil (fn [x] {:tag :nil :args '("nil")})
                :string (fn [& xs] {:tag :string :args (list (str  (str/join xs)))})
                :hex (fn [x] {:tag :number :args (list (Long/parseLong x 16))})
                
                :with-meta (fn [[_ {:keys [tag args]}]  & xs]
                             (condp = tag
                               :symbol (assoc (first xs) :type (first args))
                               :keyword (assoc (first xs) :attrs (conj (or (:attrs (first xs)) {})
                                                                       {(keyword (first args)) true}))
                               :map xs ;; Todo
                               xs))
                
                :comment (fn [& xs] {:tag :comment :args xs})}               
               tree))

(defn assemble [tree]
  (-asm @*env* tree))

(defn output [code file]
  (let [code (str/join NL code)]
    (log/debug (str NL "=== code ===" NL NL code NL NL "============"))
    (if file
      (spit file code)
      code)))


(defn add-library [env lang]
  (let [[global-env idx] (loop [e env
                                prev-e nil
                                idx 0]
                           (if-not (:parent e)
                             [e idx]
                             (recur (:parent e) e (inc idx))))
        nses (concat (->> global-env
                          :symbols
                          (filter :load?)
                          (map :ns)
                          distinct)
                     (->> env
                          (clojure.walk/postwalk
                           (fn [x] (if (and (map? x) (:requires x)) (:requires x))))
                          (keep :ns)
                          (distinct)))]
    (if-not (seq nses)
      env
      (as-> env env#
        (reduce (fn [e x]
                  (update e :code
                          concat
                          (->> (:symbols global-env)
                               (filter #(and (:load? %)
                                             (= x (:ns %))))
                               (map #(str/join  NL [(str "-- " (:symbol %) " --")
                                                    (str (:ns %) "." (:symbol %) "=" (:fn %))
                                                    "--------------" NL])))
                          
                          ;; (->> (:requires e)
                          ;;      (map (fn [{:keys [alias ns]}]
                          ;;             (str (or alias ns) "=" "require(" ns ")"))))
                          ;; [NL
                          
                          ;;  (let [[x# & xs#] (str/split (str x) #"\.")]
                          ;;    (str x# "="
                          ;;         (reduce (fn [r z] (str  "{" z "=" r "}")) "{}" (reverse xs#))))]
                          ))
                env#
                nses)
        (update env# :code concat [NL] (-require-library lang nses) [NL]))))) ;; built-in nses

(defn ns-postwalk [nses]
  (reduce (fn [r e]
            
            )
          #{}
          nses)
  
  )
(defn timestamp [env]
  (update env :code concat [(str "-- Transpiled by Spiler at " (datetime "YYYY-MM-dd HH:mm:00") NL)]))


(defn parse [text & {:keys [lang file load-built-in?] :or {lang :lua load-built-in? true}}]
  (require (symbol (str "spiler." (name lang))))
  (binding [*env* (atom (-> (create-env)
                            (merge {:symbols (concat built-in-macros (-built-in lang))
                                    :ns "base"
                                    :language lang})))]
    
    (let []
      (-> (bnf/parser text)
          (expand-builtin-macro)
          (create-node-tree)
          (assemble)
          (add-library lang)
          (timestamp)
          (generate-code)
          (output file)))))

(defn gen-code [file-path]
  (->> file-path
       (slurp)
       (parse)
       (spit (str/replace (.getName (io/as-file file-path)) #".clj$" ".lua"))))

(def options-schema
  [["-f" "--file <file>" "A clj file to be parsed"]
   ["-d" "--debug" "Print debug message"]
   [nil "--help" "This help"]])

(defn -main [& args]
  (let [{:keys [options arguments summary]} (parse-opts args options-schema)]
    (log/set-level! (if (:debug options) :debug :info))
    (if (or (:help options) (not (:file options)))
      (println (str NL "Usage: " "spiler " "<options>" NL NL " Options:" NL  summary))
      (if-let [clj (:file options)] 
        (println (parse (slurp clj)))
        (println (parse (first arguments)))))))







