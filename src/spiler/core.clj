(ns spiler.core
  (:require [clojure.string :as str]))

(defrecord Env [ns symbols requires language])

(defn create-env
  ([]
   (map->Env {:symbols '()
              :imports '()
              :requires '()
              :code '()}))
  
  ([{:keys [ns parent]}]
   (map->Env {:symbols '()
              :imports '()
              :requires '()
              :parent parent
              :ns (or ns (:ns parent))})))

(def NL "\n")

(def ^:dynamic *warn-on-no-symbol* false)

(def ^:dynamic *env* (atom (create-env)))




(defmulti -asm (fn [env {:keys [tag]} & [ret]] tag))
(defmulti -built-in (fn [lang] lang))
(defmulti -require-library (fn [lang & _] lang))

(defn lookup* [sym coll]
  (filter #(= (:symbol %) (symbol sym)) coll))

(defmacro ret* [env]
  `(-> ~env :ret :v))

(defmacro wrap-ret [ret & body]
  `(let [xs# (str/join NL (butlast (vector ~@body)))
         ret-body# (last (vector ~@body))]
     (str xs# (when ~ret (str ~ret "=")) ret-body#)))

(defn and+ [separator coll]
  (loop [vs coll ws []]
    (if-not (seq vs)
      (reduce (fn [r [x0 x1]]
                (if x1
                  (str r (when (seq r) " and ") "(" x0 separator x1 ")")
                  r))
              ""
              ws)
      (recur (rest vs)
             (conj ws [(first vs)
                       (second vs)])))))


(defn generate-code [{:keys [code]} & libs]
  (loop [code code
         result '()]
    (if-not (seq code)
      result
      (if (sequential? (first code))
        (recur (concat (first code) (rest code)) result)
        (recur (rest code) (conj result (first code)))))))


(defn emit [env & codes]
  (reduce (fn [e c]
            (if c (update e :code conj c) e))
          env
          codes))








(defn stack-args [env args]
  (reduce (fn [e arg]
            (let [{ret :ret :as xe} (-asm e arg)]
              (cond-> xe
                ret  (update :stack conj ret)
                ;;                         code  (update :code conj code)
                )))
          (create-env {:parent env})
          args))





(defn deep-merge-with
  "Like merge-with, but merges maps recursively, applying the given fn
  only when there's a non-map at a particular level.
  (deep-merge-with + {:a {:b {:c 1 :d {:x 1 :y 2}} :e 3} :f 4}
                     {:a {:b {:c 2 :d {:z 9} :z 3} :e 100}})
  -> {:a {:b {:z 3, :c 3, :d {:z 9, :x 1, :y 2}}, :e 103}, :f 4}"
  [f & maps]
  (apply
   (fn m [& maps]
     (if (every? map? maps)
       (apply merge-with m maps)
       (apply f maps)))
   maps))


