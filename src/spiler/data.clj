(ns spiler.data
  (:gen-class
   :name spiler.data.Generator
   :methods [#^{:static true} [genFakeData [String, java.util.HashMap] Object]
             #^{:static true} [parse [String] Object]
             #^{:static true} [db [String] Object]])
  (:require [silvur.util :refer [edn->json json->edn]]
            [silvur.datetime2 :refer [datetime date datetime* inst< -year adjust +time
                                      -month before after vec<  -hour -minute -second -day
                                      zone-offset UTC JST]]
            [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]
            [clojure.core.match :refer [match]]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [spiler.sql :refer [sql->edn]]
            [camel-snake-kebab.core :as csk]
            [camel-snake-kebab.extras :as cske]))


(defmulti gen-random-data (fn [{col-type :type {ann-type :type} :annotation}]
                            (or ann-type col-type)))


(defmethod gen-random-data :boolean [_]
  (< 0.5 (rand)))

(defmethod gen-random-data :varchar [{:keys [column-name length table annotation] :as d}]
  (let [s (str "SAMPLE VC:" column-name)]
    (subs s 0 (min (first length) (count s)))))

(defmethod gen-random-data :enum [{{items :items} :annotation}]
  (nth items (int (* (count items) (rand)))))

(defmethod gen-random-data :uuid [_]
  (random-uuid))

(defmethod gen-random-data :integer [{{max-value :max :or {max-value Integer/MAX_VALUE}} :annotation}]
  (rand-int (inc max-value)))

(defmethod gen-random-data :decimal [{[p s] :length {[ann-p ann-s] :length} :annotation}]
  (let [precision (or ann-p p)
        scale (or ann-s s)]
    (+ (long (rand (reduce * 1 (repeat (- precision scale) 10)) ))
       (* (reduce * 1 (repeat scale 0.1))
          (rand-int (inc (reduce * 1 (repeat scale 10))) )))))

(defmethod gen-random-data :datetime [{{ann-type :type from :from
                                        unit :unit duration :duration} :annotation
                                       :as m}]
  (let [unit (keyword (str/lower-case (name (or unit :hour))))
        duration (or duration 120)]
    (-> (datetime (when from (mapv parse-long (str/split from #"[/\-:T]"))))
        (+time  ((condp = unit
                   :second -second
                   :minute -minute
                   :hour -hour
                   :day -day
                   :month -month
                   :year -year
                   -hour)
                 (long (rand-int (inc duration)))))
        (java.time.OffsetDateTime/of  java.time.ZoneOffset/UTC))))




(defmethod gen-random-data :date [{type :type {ann-type :type from :from} :annotation :as m}]
  (cond-> (gen-random-data (assoc-in m [:annotation :type] :datetime)) 
    :execute (.toLocalDate)
    (= type :varchar) str))

(defmethod gen-random-data :text [_]
  "This sentence is by text generator.")

(defmethod gen-random-data :default [e]
  #p e
  "DEFAULT SAMPLE")







;;;;;;;;;;;;;;;



;; (defonce first-names (with-open [rdr (io/reader (io/resource "first-name.csv"))]
;;                        (->> (rest (line-seq rdr))
;;                             (map #(str/split % #","))
;;                             (doall))))

;; (defonce given-names (with-open [rdr (io/reader (io/resource "given-name.csv"))]
;;                        (->> (rest (line-seq rdr))
;;                             (map #(str/split % #","))
;;                             (doall))))






;; (defmethod gen-random-data :first-name [{:keys [character]}]
;;   (let [o (condp = (keyword character)
;;             :hiragana 0
;;             :roman 1
;;             :kanji 2
;;             1)]
;;     (str/capitalize (nth (nth first-names (rand-int (count first-names)) ) o))))

;; ;; Given name
;; (defmethod gen-random-data :given-name [{:keys [character]}]
;;   (let [o (condp = (keyword character)
;;             :hiragana 0
;;             :roman 1
;;             :kanji 2
;;             1)]
;;     (str/capitalize (nth (nth given-names (rand-int (count given-names)) ) o))))

;; ;; Human name
;; (defmethod gen-random-data :human-name [{:keys [character]}]
;;   (let [o (condp = (keyword character)
;;             :hiragana 0
;;             :roman 1
;;             :kanji 2
;;             1)
;;         first-name (gen-random-data {:type :first-name :character character})
;;         given-name (gen-random-data {:type :given-name :character character})]
;;     (condp = character
;;       :roman (str first-name " " given-name)
;;       (str given-name " " first-name))))


;; ;; ;; Specific type
;; (defmethod gen-random-data :uuid [_]
;;   (str (random-uuid)))

;; (defmethod gen-random-data :enum [{name :name
;;                                    {:keys [collection]} :annotation}]
;;   (nth collection (int (* (count collection) (rand)))))



;; (defmethod gen-random-data :datetime [{:keys [attribute annotation table] }]
;;   (let [{:keys [from unit duration] :or {unit :hour duration 128}} annotation
;;         unit (keyword (str/lower-case (name unit)))]
;;     (+time (datetime (when from (mapv parse-long (str/split from #"[/\-:T]"))))
;;            ((condp = unit
;;               :second -second
;;               :minute -minute
;;               :hour -hour
;;               :day -day
;;               :month -month
;;               :year -year
;;               -hour)
;;             (long (rand-int duration))))))


;; (defmethod gen-random-data :date [args]
;;   (cond-> (gen-random-data (assoc-in args [:annotation :type] "datetime")) 
;;     :execute (.toLocalDate)
;;     (= type :varchar) str))


;; (defmethod gen-random-data :timestamp [{:keys [from-year to-year]
;;                                         :as args
;;                                         :or {from-year 2000}}]
;;   (-> (gen-random-data (assoc args :type :datetime))
;;       (+time (java.time.Duration/of (* 1000 (rand)) (java.time.temporal.ChronoUnit/MILLIS)))))


;; ;; ;; General type


;; (defmethod gen-random-data :boolean [_]
;;   (< 0.5 (rand)))

;; (defmethod gen-random-data :integer [{:keys [annotation]}]
;;   (let [{:keys [max min] :or {max Integer/MAX_VALUE min 0}} annotation]
;;     (int (+ min (* (- max min) (rand))))))

;; (defmethod gen-random-data :tinyint [{:keys [attribute]}]
;;   (let [{:keys [max min] :or {max 127 min -128}} attribute]
;;     (inc (+ min (* max (int (/ (rand-int 255) 255.0)))))))

;; (defmethod gen-random-data :smallint [{:keys [attribute]}]
;;   (let [{:keys [max min] :or {max 10000 min 0}} attribute]
;;     (int (+ min (* (- max min) (rand))))))

;; (defmethod gen-random-data :fn [{{:keys [f]} :annotation :as args}]
;;   (f args))





(defmulti manipulate-table (fn [db {:keys [op]}] op))
(defmethod manipulate-table :create-table [db {:keys [table-name columns
                                                      foreign-keys
                                                      primary-keys]}]
  (cond-> (fn [r {a :action col :column-name e :element :as m}]
            (assoc-in r [:tables (keyword table-name) :columns (keyword col)] m))
    :run (reduce db columns)
    (seq foreign-keys) (assoc-in [:tables (keyword table-name) :foreign-keys] foreign-keys)
    (seq primary-keys) (assoc-in [:tables (keyword table-name) :primary-keys] primary-keys)))

(defmethod manipulate-table :alter-table [db {:keys [table-name columns
                                                     foreign-keys
                                                     primary-keys
                                                     annotations]}]
  (let [table (keyword table-name)]
    (cond-> (fn [r {a :action column-name :column-name :as e}]
              (let [col (keyword column-name)]
                (condp = a
                  :add (update-in r [:tables table :columns] assoc col e)
                  :add-column (update-in r [:tables table :columns] assoc col e)
                  r)))
      :run (reduce db columns)
      (seq foreign-keys) (update-in [:tables table :foreign-keys] (comp vec concat) foreign-keys)
      (seq primary-keys) (update-in [:tables table :primary-keys] (comp vec concat) primary-keys)
      (seq annotations) (update-in  [:tables table :columns]
                                    (fn [cols-map]
                                      (reduce (fn [rm {col :column-name :as ann}]
                                                (update rm (keyword col)  assoc :annotation (dissoc ann :action :column-name :element)))
                                              cols-map
                                              annotations))))))


(defmethod manipulate-table :default [db _] db)


(defn gen-db 
  ([table-ops]
   (->> table-ops
        (reduce manipulate-table  {})))
  ([table-maps post-fn]
   (-> (gen-db table-maps)
       (post-fn))))


(defn gen-fake-data [db & {:as number-or-map}]
  (cond-> (:tables db)
    (map? number-or-map) (->> (filter (fn [[k m]] (or (number-or-map k) (number-or-map :all))))
                              (map (fn [[k m]] [k (assoc m :__count (or (number-or-map k)
                                                                        (number-or-map :all)))])))
    (number? number-or-map) (->> (map (fn [[k m]] [k (assoc m :__count number-or-map)])))
    :run (->> (reduce (fn [r0 [tbl {:keys [__count columns]}]]
                        (if-not columns
                          r0
                          (assoc r0 tbl (->> columns
                                             (reduce (fn [r1 [col m]]
                                                       (assoc r1 col (gen-random-data m)))
                                                     {})
                                             (fn [])
                                             (repeatedly)
                                             (take __count)
                                             (vec)))))
                      {}))))

(defn ez-sample [file-name & [raw?]]
  (cond-> (sql->edn (slurp (io/file "resources/samples" file-name)) :raw? (some? raw?))
    (not (some? raw?)) (gen-db)))


(defn -genFakeData [sql {:as hm}]
  (-> sql
      (sql->edn)
      (gen-db)
      (gen-fake-data (if (number? hm) {:all (or hm 1)} (cske/transform-keys keyword (into {} hm))))
      (edn->json)))

(defn -parse [sql]
  (try
    (-> sql
        (sql->edn)
        (edn->json))
    (catch Exception e (throw (Exception. (ex-info (ex-message e) {}))))))
