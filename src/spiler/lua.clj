(ns spiler.lua
  (:require [spiler.core :refer :all]
            [silvur.log :as log]
            [clojure.string :as str]
            [clojure.pprint :refer (pprint)]
            [taoensso.timbre :as timbre]))


(def ^:dynamic *lookup-symbols* true)

(defmethod -built-in :lua [_]
  [{:ns 'clojure.core
    :symbol '+
    :macro? false
    :fn (fn [ret & xs] (wrap-ret ret (str/join "+" xs)))}
   {:ns 'clojure.core
    :symbol '- 
    :fn (fn [ret & xs] (wrap-ret ret (str/join "-" xs)))}
   {:ns 'clojure.core
    :symbol '*
    :fn (fn [ret & xs] (wrap-ret ret (str/join "*" xs)))}
   {:ns 'clojure.core
    :symbol '/ 
    :macro? false
    :fn (fn [ret & xs] (wrap-ret ret (str/join "/" xs)))}
   {:ns 'clojure.core
    :symbol '<= 
    :fn (fn [ret & xs] (wrap-ret ret (and+ "<=" xs)))}
   {:ns 'clojure.core
    :symbol '< 
    :fn (fn [ret & xs] ((wrap-ret ret and+) "<" xs))}
   {:ns 'clojure.core
    :symbol '> 
    :fn (fn [ret & xs] (wrap-ret ret (and+ ">" xs)))}
   {:ns 'clojure.core
    :symbol '>= 
    :fn (fn [ret & xs] (wrap-ret ret (and+ ">=" xs)))}
   {:ns 'clojure.core
    :symbol '= 
    :macro? false
    :fn (fn [ret & xs] (wrap-ret ret (and+ "==" xs)))}
   {:ns 'clojure.core
    :symbol 'first 
    :fn (fn [ret x] (wrap-ret ret (str x "[1]"))) } ;; Lua uses the index from 1}
   {:ns 'clojure.core
    :symbol 'last 
    :fn (fn [ret x] (wrap-ret ret (str x "[" "#" x "]")))}
   {:ns 'clojure.core
    :symbol 'vec 
    :fn (fn [ret & xs] (wrap-ret ret (str "{" (str/join "," xs) "}")))}
   {:ns 'clojure.core
    :symbol 'inc 
    :fn (fn [ret x] (wrap-ret ret (str x "+1")))}
   {:ns 'clojure.core
    :symbol 'dec 
    :fn (fn [ret x] (wrap-ret ret (str x "-1")))}
   {:ns 'clojure.core
    :symbol 'set! 
    :fn (fn [ret x v] (str x "=" v))}
   {:ns 'clojure.core
    :symbol 'str 
    :fn (fn [ret & xs] (wrap-ret ret (str "(function() return " "\"\".." (str/join ".." xs) " end)()")))}
   {:ns 'clojure.core
    :symbol 'count 
    :fn (fn [ret & xs] (wrap-ret ret  (str "#" (first xs))))}
   {:ns 'clojure.core
    :symbol 'println 
    :macro? false
    :fn (fn [ret & xs] (str  "print" "(" (str/join "," xs) ")"))}
   {:ns 'clojure.core
    :symbol 'atom 
    :macro? false
    :fn (fn [ret x]
          (wrap-ret ret
                    (str "{" "_type=" "\"atom\"" "," "_value=" x "}")))}
   {:ns 'clojure.core
    :symbol 'deref 
    :macro? false
    :fn (fn [ret x]
          (wrap-ret ret (str x "[\"_value\"]")))}
   {:ns 'clojure.core
    :symbol 'reset! 
    :macro? false
    :fn (fn [ret & xs]
          (let [[v nv] xs]
            (wrap-ret ret
                      (str v "[\"_value\"]=" nv)
                      v)))}   
   {:ns 'clojure.core
    :symbol 'swap! 
    :macro? false
    :fn (fn [ret & xs]
          (let [[v f & args] xs]
            (wrap-ret ret
                      (str v "=" f "(" v "," (str/join "," args) ")")
                      v)))}

   {:ns 'clojure.core
    :fn (str/join NL (list
                      "function(x)"
                      "local f,err=load(x)"
                      "if (f==nil)"
                      "then"
                      "return err"
                      "else"
                      "if pcall(f)"
                      "then"
                      "return f()"
                      "else"
                      "return \"Runtime error\""
                      "end"
                      "end"
                      "end"))
    :symbol 'eval
    :load? true}
   
   {:ns 'clojure.core
    :fn (str/join NL (list
                      "function(x, coll)" 
                      "local t = {x}" 
                      "for i,v in ipairs(coll) do" 
                      "t[i+1]=v"
                      "end"
                      "return t" 
                      "end"))
    :symbol 'cons
    :load? true}

   {:ns 'clojure.core
    :fn (str/join NL (list
                      "function(coll, x)" 
                      "local t = {}" 
                      "for i,v in ipairs(coll) do" 
                      "t[i]=v"
                      "end"
                      "table.insert(t, x)"
                      "return t" 
                      "end"))
    :symbol 'conj
    :load? true}

   {:ns 'clojure.string
    :fn (str/join NL (list
                      "function(coll, x)" 
                      "local t = {}" 
                      "for i,v in ipairs(coll) do" 
                      "t[i]=v"
                      "end"
                      "table.insert(t, x)"
                      "return t" 
                      "end"))
    :symbol 'join
    :load? true}])


(defn require-list->tree [coll]
  ;; ["clojure.lua.io" "clojure.string"]
  (->> coll
       (map #(str/split (name %) #"[.]"))
       (reduce (fn [r z]
                 (conj r (reduce (fn [m x]
                                   (assoc {} (symbol x) m))
                                 {}
                                 (reverse z))))
               [])
       (apply deep-merge-with conj)

       (map (fn [[k m]]
              (str k "=" (clojure.walk/postwalk
                          (fn [x]
                            (cond
                              (map? x) (str "{" (str/join " , " (map #(str/join "=" %) x)) "}")
                              :else x))
                          m))))))



(defn- lookup [env sym]
  (let [sym (symbol sym)
        [fname ns] (if (= '/ sym)
                     ["/" nil]
                     (reverse (str/split (str sym) #"/")))]
    (if ns
      (loop [e env]
        (if (nil? (:parent e))
          (let [[n] (filter (fn [x] (or (= (symbol ns) (:ns x))
                                        (= (symbol ns) (:alias x))))
                            (:requires e))]
            (when-not n
              (and *warn-on-no-symbol* (log/warn "No name space: " ns)))
            
            (str (str (or n ns) ".") fname))
          (recur (:parent e))))
      (loop [e env]
        (if-not e
          (let [{:keys [v fn load?]} (first (lookup* fname (:symbols @*env*)))]
            (or v fn (do (and *warn-on-no-symbol* (log/warn "No symbol: " sym))
                         fname)))
          (let [{:keys [ns symbol v fn load?]} (first (lookup* fname (:symbols e)))]
            ;; (or fn v (recur (:parent e)))
            (cond
              load? (str ns "." symbol)
              (and (not load?) (some? fn)) fn
              (and (not load?) (some? v)) v
              :else (recur (:parent e)))
            ))))))

(defmethod -asm :S [env {:keys [args]}]
  (let [{:keys [ns] :as ne} (stack-args env args)]
    (emit ne (str NL "return " (or ns (ret* ne))))))


(defmethod -asm :ns [env {:keys [args]}]
  (binding [*lookup-symbols* false]
    (let [ns-e (-asm env (first args))
          ne (reduce (fn [e arg]
                       (-asm e arg))
                     env
                     args)]
      (cond-> env
        true (assoc :ns (ret* ns-e))
        true (update :requires concat (:requires ne))
        (ret* ns-e) (emit 
                     (loop [nary (reverse (str/split (str (ret* ns-e)) #"[.]"))
                            result ""]
                       (if-not (seq nary)
                         (str result NL)
                         (recur (rest nary) (str (first nary) "=" "{" result  "}")))))
        (:code ne) (emit (:code ne))))))





(defmethod -asm :fn [env {:keys [args body]} & [ret]]
  (let [rets (repeatedly (count args) #(gensym "F_"))
        ret (or ret (gensym "F_"))
        be (-> (reduce (fn [e [arg ret]]
                         (-asm e {:tag :binding
                                  :args (list arg {:tag :symbol :args (list ret)})}))
                       (create-env {:parent env})
                       (map #(vector %1 %2) args rets))
               (-asm body))]

    (-> env
        (assoc :ret {:v ret :tag :fn})
        (emit (str ret "=" "function(" (str/join "," rets) ")")
              ;; stack has the return value at last position
              (str/join NL (generate-code be))
              (str "return " (ret* be))
              "end"))))

(defmethod -asm :if [env {:keys [condition t-body f-body]}]
  (let [ce (-asm (create-env {:parent env}) condition)
        te (-asm (create-env {:parent env}) t-body)
        fe (-asm (create-env {:parent env}) f-body)
        ret (gensym "I_")]


    (-> env
        (emit (:code ce))
        (emit (str "if" "(" (ret* ce) ")")
              (str "then")
              (:code te)
              (str ret "=" (ret* te))
              (str "else")
              (:code fe)
              (str ret "=" (ret* fe))
              (str "end"))
        (assoc :ret {:v ret :tag :if}))))


(defmethod -asm :let [env {:keys [bindings body]}]
  (let [be (-> (reduce (fn [e arg]
                         (-asm e arg))
                       (create-env {:parent env})
                       bindings)
               (-asm body))]

    (-> env
        (assoc :ret {:v (ret* be) :tag :let})
        (update :code conj (:code be)))))

(defmethod -asm :body [env {:keys [args]}]
  (let [ne (reduce (fn [e arg]
                     (-asm e arg))
                   (create-env {:parent env})
                   args)]
    
    (cond-> env
      true (assoc :ret {:v (ret* ne) :tag :body})
      (:code ne) (update :code conj (:code ne)))))



(defmethod -asm :binding [env {[s v] :args}]

  ;; binding
  ;;        |-- destructuring-map :- :stack ({:v a :idx a} {:v b :idx b})
  (let [nse (binding [*lookup-symbols* false]
              (-asm (create-env {:parent env}) s))
        nve (-asm nse v)

        ne (reduce (fn [e arg]
                     (cond-> e
                       (:key arg) (update :code conj (str "local " (:v arg) "=" (ret* e) "[" "\"" (:key arg) "\"" "]"))
                       (:idx arg) (update :code conj (str "local " (:v arg) "=" (ret* e) "[" (:idx arg) "]"))                       
                       true (update :symbols conj {:ns (:ns e)
                                                   :symbol (symbol (:v arg))
                                                   :v (:v arg)})
                       ;; (assoc-in [:symbols (:v arg)] (:v arg))
                       ))
                   (cond-> nve
                     (ret* nse) (update :code conj (str "local " (ret* nse) "=" (ret* nve)))
                     (ret* nse) (update :symbols conj {:ns (:ns nve)
                                                       :symbol (symbol (ret* nse))
                                                       :v (ret* nse)})
                     ;; (assoc-in [:symbols (ret* nse)] (ret* nse))
                     )
                   (:stack nse))]

    (-> env
        (update :symbols conj (:symbols ne))
        (update :code conj (:code ne))
        (update :stack concat (:stack ne))
        (update :stack empty))))

(defmethod -asm :ns-require [env {:keys [args]}]
  ;; {:tag :ns-vector, :args ({:tag :symbol, :args (clojure.lua.io)} {:tag :keyword, :args ("as")} {:tag :symbol, :args (a)})}
  (let [ae (reduce (fn [e arg]
                     (let [x (-asm e arg)]
                       (update e :stack concat (:stack x))))
                   env
                   args)
        built-in-ns (set (map :ns (-built-in :lua)))
        ne (reduce (fn [e {:keys [v as]}]
                     (let [sv (symbol v)]
                       (cond-> e
                         :always (update :requires conj {:alias as :ns sv})
                         (not (built-in-ns sv)) (update :code conj (str (or as sv) "=" "require(" "\"" v "\"" ")"))
                         ;;(update :imports conj {:name (symbol v) :as (when as (symbol as))})
                         )))
                   ae
                   (:stack ae))]
    (update ne :stack empty)))

(defmethod -asm :ns-import [env {:keys [args]}]
  (let [ae (reduce (fn [e arg]
                     (let [x (-asm e arg)]
                       (update e :stack concat (:stack x))))
                   env
                   args)

        ne (reduce (fn [e {:keys [v as]}]
                     (-> e
                         ;;(update :code conj (str (or as v) "=" "require(" "\"" v "\"" ")"))
                         (update :imports conj {:name (symbol v)})
                         (doto clojure.pprint/pprint)
                         ))
                   ae
                   (:stack ae))]

    (-> ne
        (update :stack empty))))


(defmethod -asm :destructuring-symbol [env {[s] :args}]
  (let [{{v :v} :ret} (-asm env s)
        x {:v v :tag :destructuring-symbol}]
    (-> env
        ;; (assoc :ret {:v s :tag :destructuring-symbol})
        (assoc :ret x)
        (update :stack conj x)
        )))


(defmethod -asm :ns-vector [env {:keys [args]}]
  (loop [e (create-env (:parent env))
         xs args
         x '()
         as? false]
    (let [{{v :v} :ret} (-asm e (first xs))
          [ns# & classes-or-ns-suffixes] (reverse x)]
      (cond 
        (nil? v) (if (seq classes-or-ns-suffixes)
                   (reduce (fn [re z]
                             (update re :stack conj {:v (str/join "." [ns# z])}))
                           e
                           classes-or-ns-suffixes)
                   (update e :stack conj {:v ns#}))
        as? (-> e
                (assoc :v v)
                (update :stack conj {:v (str/join "." x) :as v}))
        (= "as" v) (recur e (rest xs) x true)
        :else (recur e (rest xs) (conj x v)  false)))))

(defmethod -asm :destructuring-vector [env {:keys [args]}]
  (loop [e (create-env (:parent env))
         xs args
         idx 1
         as? false]
    ;; Lua uses the index from 1
    (let [{{v :v} :ret} (-asm e (first xs))]
      (cond 
        (nil? v) e
        as? (assoc e :ret {:v v :tag :destructuring-vector})
        (= "as" v) (recur e (rest xs) idx true)
        :else (recur (update e :stack conj {:v v :idx idx}) (rest xs) (inc idx) false)))))

(defmethod -asm :destructuring-map [env {:keys [args]}]
  (let [ne (reduce (fn [e [{tag :tag :as s} v]]
                     (let [{{x :v} :ret} (-asm e s)]
                       (cond
                         (and (= tag :keyword) (= x "keys"))
                         (update e :stack concat (map (fn [arg]
                                                        (let [{z :ret} (-asm e arg)]
                                                          {:v (:v z) :key (:v z) }))
                                                      (:args v)))

                         (and (= tag :keyword) (= x "as"))
                         (assoc-in e [:ret :v] (ret* (-asm e v)))

                         (and (= tag :keyword) (= x "or"))
                         (reduce (fn [me [mk mv]]
                                   (update me :code conj (str "local " (ret* (-asm e mk)) "=" (ret* (-asm e mv)))))
                                 e
                                 (partition 2 (:args v)))

                         (= tag :symbol) (update e :stack conj {:v x :key (ret* (-asm e v))}))))
                   (create-env {:parent env})
                   (partition 2 args))]


    (-> env
        (assoc :ret {:v (ret* ne) :tag :destructuring-map})
        (update :code concat (:code ne))
        (update :stack concat (:stack ne)))))



(defmethod -asm :def [env {[s v] :args}]
  (binding [*warn-on-no-symbol* false]
    (let [se (-asm (create-env {:parent env}) s)
          x (ret* se)
          declare-x (gensym "D_")
          ns (:ns se)
          ;; Specify return variable if tag is :fn for recursive function.
          ;; It is unnecessary for other tags

          ve (apply -asm (-> (create-env {:parent env})
                             (update :symbols conj {:ns ns :symbol (symbol x) :v declare-x})
                             ;;(assoc-in [:symbols (symbol x)] {:v declare-x :ns ns})
                             )
                    v
                    (when (= :fn (:tag v)) [declare-x]))

          v (ret* ve)
          ;; ne (stack-args env args)
          ;; [x v] (map :v (reverse (:stack ne)))

          sym (symbol (str (when ns (str ns "."))  x))]

      ;;(swap! *env* assoc-in [:symbols (symbol x)] {:v sym :ns ns})
      (swap! *env* update :symbols conj {:ns ns
                                         :symbol (symbol x)
                                         :v sym })

      (-> env
          (assoc :ret {:v x :tag :def})
          (update :code conj (:code ve))
          (update :code conj (str sym "=" v))))))





(defmethod -asm :quote [env {:keys [args]}]
  (-asm env (first args))
  ;; (let [ne (stack-args env args)]
  ;;     (-> env
  ;;         (update :code conj (:code ne))
  ;;         (update :code conj (str/join "," (map :v (:stack ne))))

  ;; ))
  )


(defmethod -asm :quote-list [env {:keys [args]}]
  (let [ne (stack-args env args)
        ret (gensym "QL_")]
    (-> env
        (assoc :ret {:v ret :tag :quote-list})
        (update :code conj (:code ne))
        (update :code conj (str ret "=" "{" (str/join "," (map :v (reverse (:stack ne)))) "}")))))


(defmethod -asm :list [env {:keys [args]}]
  (let [ne (stack-args env args)
        [{f :v ftag :tag :as x} & opts] (reverse (:stack ne))
        ret (gensym "L_")]

    (cond-> env
      true (assoc :ret {:v ret :tag :list})
      (:code ne) (update :code conj (:code ne))
      ;; Call function
      (= :symbol ftag) (update :code conj (if (fn? f)
                                            (apply f ret (map :v opts))
                                            (str ret "=" f "(" (str/join "," (map :v opts)) ")")))

      ;; Call method
      (= :method ftag) (update :code conj (let [obj (:v (first opts))
                                                method (str/replace (str f) #"^[.]" "")]
                                            (str ret "=" obj ":" f "(" (str/join "," (map :v (rest opts))) ")")))

      ;; Call directly as function
      (= :list ftag) (update :code conj (if (fn? f)
                                          (apply f ret (map :v opts))
                                          (str ret "=" f "(" (str/join "," (map :v opts)) ")")))
      
      ;; Call by keyword
      (= :keyword ftag) (update :code conj (str (:v (first opts)) "[\"" f "\"]" ))
      ;; Call by map
      (= :map ftag) (update :code conj (str f "[" (:v (first opts))  "]" ))

      
      ;;      true (update :stack empty)
      ;;      true (update :stack conj {:v ret :tag :list})
      )))


(defmethod -asm :symbol [env {[v] :args :as x}]
  (let [s (if *lookup-symbols* (lookup env v) v)
        ret (gensym "S_")]
    (-> env
        (assoc :ret {:v (if-not (or (fn? s) (re-find #"-[0-9]+" (str s)))
                          (-> s str
                              (str/replace #"->" "_right_arrow_")
                              (str/replace #"->" "_left_arrow_")
                              (str/replace #"-" "__"))
                          s)
                     :tag :symbol})
        ;;        (update :code conj (str ret "=" s))
        )))


(defmethod -asm :map [env {args :args}]
  (let [ne (stack-args env args)
        ret (gensym "M_")
        code (str "{" (->> ne
                           :stack
                           reverse
                           (partition 2)
                           (map (fn [[{k :v} {v :v}]] (str k "=" v)))
                           (str/join ","))
                  "}")]

    (cond-> env
      true (assoc :ret {:v ret :tag :map})
      (:code ne) (update :code conj (:code ne))
      true (update :code conj (str ret "=" code)))))

(defmethod -asm :vector [env {args :args}]
  (let [ne (stack-args env args)
        ret (gensym "V_")
        code (str "{" (->> ne
                           :stack
                           reverse
                           (map (fn [{v :v}] v))
                           (str/join ","))
                  "}")]

    (cond-> env
      true (assoc :ret {:v ret :tag :map})
      (:code ne) (update :code conj (:code ne))
      true (update :code conj (str ret "=" code)))))


(defmethod -asm :string [env {tag :tag [v] :args}]
  (-> env
      (assoc :ret {:v (str "\"" v "\"") :tag tag})))

(defmethod -asm :default [env {tag :tag [v] :args}]
  (-> env
      (assoc :ret {:v v :tag tag})))


(defmethod -require-library :lua [lang & nses]
  (apply require-list->tree nses))
