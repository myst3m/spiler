(ns spiler.sql
  (:require [clj-antlr.core :as antlr]
            [silvur.util :refer [edn->json json->edn]]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.walk :as w]
            [clojure.core.match :as m :refer [match]]
            [camel-snake-kebab.core :as csk]
            [camel-snake-kebab.extras :as cske]
            [clojure.core.memoize :as mem]
            [hashp.core :refer [p*]]
            ))


(def sql-parser (antlr/parser (slurp (io/resource "SQL.g4"))
                              {:case-sensitive? false}))

(def memoized->kebab-case-keyword
  (mem/fifo csk/->kebab-case-keyword {} :fifo/threshold 512))

(defn transform
  ([ast]
   (transform {} ast))
  ([m ast]
   (->> ast
        (w/postwalk (fn [xs]
                      (if (and (seq? xs) (keyword (first xs)))
                        (if-let [f (m (first xs))]
                          (f xs)
                          xs)
                        xs)))
        (cske/transform-keys memoized->kebab-case-keyword))))



(defn debug-item [& xs]
  (prn xs)
  xs)



(def mapper {;; primitive
             :any_name (comp str/join rest)
             :string_wrap (fn [xs]
                            (last (re-find #"^'(.*)'$"(last xs) )))
             :signed_number (fn [xs]
                              (let [x (str/join (rest xs))]
                                (or (parse-long x) (parse-double x))))
             ;; 2nd level
             :name (partial apply hash-map)
             ;; Meaning
             :type_name (fn [xs]
                          (let [[{n :name} & rs]  (filter map? xs)
                                length (filterv number? xs)
                                n (csk/->kebab-case-keyword (str/lower-case n))]
                            (if (seq length)
                              {:type n :length length}
                              {:type n})
                            
                            ;; (match (str/upper-case n)
                            ;;        "VARCHAR" {:type "VARCHAR" :length length}
                            ;;        "DECIMAL" {:type "DECIMAL" :length length}
                            ;;        :else (if (seq length)
                            ;;                {:type n :length length}
                            ;;                {:type n}))
                            ))

             
             :column_name (partial apply hash-map)
             :table_name (partial apply hash-map)             
             :column_def (fn [xs]
                           (->> (filterv map? xs)
                                (apply merge-with (comp vec flatten (partial conj [])))
                                (conj {:element :column})))
             :table_item_def (fn [xs]  (reduce conj (filterv map? xs)))
             :create_table_stmt (fn [xs]
                                  (let [[m & col-defs] (filter map? xs)]
                                    (-> (into m (group-by :element col-defs))
                                        (clojure.set/rename-keys {:column :columns
                                                                  :foreign-key :foreign-keys
                                                                  :primary-key :primary-keys
                                                                  :annotation :annotations})
                                        (assoc :op :create-table))))
             :sql_stmt (fn [xs]
                         (->> xs (filter map?) (reduce conj)))
             :sql_stmt_list (fn [xs]
                              (->> xs (filterv map?)))
             :parse second
             

             :alter_table_stmt (fn [xs]
                                 (let [[m & col-defs] (filter map? xs)]
                                   (-> (into m (group-by :element col-defs))
                                       (clojure.set/rename-keys {:column :columns
                                                                 :foreign-key :foreign-keys
                                                                 :primary-key :primary-keys
                                                                 :annotation :annotations
                                                                 })
                                       (assoc :op :alter-table))
                                   ;; (-> (assoc m :columns (vec col-defs))
                                   ;;      (assoc :op :alter-table))
                                   ))

             :annotation_uuid_clause (fn [xs]
                                       {:type :uuid}
                                       )
             :annotation_enum_clause (fn [xs]
                                       (assoc (->> xs
                                                   (filter map?)
                                                   (reduce (fn [r x]
                                                             (-> (merge-with vector r x)
                                                                 (update :items (comp vec flatten vector))))
                                                           {}))
                                              :type :enum))
             :annotation_general_clause (fn [xs]
                                          (->> xs (filter map?) (reduce conj)))
             
             :annotation_clause (fn [xs] (->> xs
                                              (filter map?)
                                              (reduce conj {:element :annotation})))
             
             :annotation_number_option (fn [xs]
                                         {(csk/->kebab-case-keyword (str/lower-case (second xs))) (last xs)})
             :annotation_enum_item (fn [xs]
                                     {:items (last xs)})
             :alter_table_item (fn [xs] (->> xs (filter map?) (reduce conj)))
             :alter_action (fn [xs] {:action (keyword (str/lower-case (str/join "-" (rest xs))))})
             
             :literal_value (fn [xs] (second xs))
             
             :column_constraint (fn [xs]
                                  {:constraint [(str/join " " (rest xs))]})
             ;; Index
             :indexed_column (fn [xs]
                               (second xs))
             :auto_increment_clause (fn [xs] {:auto_increment (parse-long (last xs))})

             :foreign_table (fn [xs] {:table_name (last xs)})
             :foreign_key_clause (fn [xs]
                                   {:references (->> xs
                                                     (filter map?)
                                                     (reduce (fn [r x]
                                                                    (-> (merge-with vector r x)
                                                                        (update :column_name (comp vec flatten vector))))))})
             :foreign_key_constraint (fn [xs]
                                       {:element :foreign-key
                                        :foreign_key (->> (filterv map? xs)
                                                          (reduce (fn [r x]
                                                                    (-> (merge-with vector r x)
                                                                        (update :column_name (comp vec flatten vector))))))})
             :primary_key_constraint (fn [xs]
                                       {:element :primary-key
                                        :primary_key (->> (filterv map? xs)
                                                          (reduce (fn [r x]
                                                                    (-> (merge-with vector r x)
                                                                        (update :column_name (comp vec flatten vector))))))})})

(defn sql->edn [sql & {:keys [raw?]}]
  (transform (if raw? {} mapper)  (antlr/parse sql-parser sql)))
