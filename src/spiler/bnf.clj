(ns spiler.bnf
  (:require [instaparse.core :as i :refer (defparser)]))


(def grammer
  "S = (exp|<ws>)* 
    <exp> = literal|containers|form
    <form> = defn-form|quote-form|deref-form|syntax-quote-form|let-form|def-form
            |lambda-form|fn-form
            |unquote-form|unquote-splicing-form|var-quote-form|ns-form
            |do-form|doto-form|if-form|when-form
            |arrow-1-form|arrow-2-form


    (***** atom *****)

    open-paren = '('
    close-paren = ')'
    ampersand = '&'
    left-square-bracket = '['
    right-square-bracket = ']'
    left-curly-bracket = '{'
    right-curly-bracket = '}'  
    circumflex = '^'
    commercial-at = '@'
    number-sign = '#'
    apostrophe = #'[\\']'

    backslash = '\\\\'
    double-quote = '\\\"'
    percent = '%'

    (***** token *****)

    <number> = decimal
             | hex
             | octal
             | binary

    decimal = '-'? (#'[0-9]+[.]*[0-9]*([eE]-?[0-9]+)*')
    binary = #'[0-9]+' ('r'|'R') #'[0-9a-fA-F]+'
    hex = ('-'? <'0'> <('x'|'X')> #'[0-9a-fA-F]+')
    octal = <'0'> #'[0-7]+'
    unicode-escape = '\\\\' 'u' hex hex hex hex

    character = ( '\\\\newline' | '\\\\space' | '\\\\tab' | '\\\\u' hex hex hex hex | backslash  #'.')

    <escape-sequence> = #'\\\\[tnr]'
    syntax-quote = '`'
    unquote-splicing = '~@'
    unquote = '~'

    (* skipped *)
    comment = ';' #'[^\\r\\n]*' ('\\r'? '\\n')?
    ws = #'[ \t\n\r,]+' | comment


    (***** types *****)

    nil = 'nil'
    boolean = 'true' | 'false'
    string = <double-quote> (escape-sequence | #'\\\\\"' | #'[^\"]' | #'[ \t\n\r,]+')*  <double-quote>
    regex = <number-sign> <'\"'> ( #'[^\"]' | #'\\\\' '.')* <'\"'>
    keyword = <':'> <(':')?> #'[^\"\t\n\r,(): 0-9\\[\\]{}]+'
    symbol = #'[^^\"\t\n\r,(): \\[\\]{}]+'


    (***** containers *****)

    list = <open-paren> (<ws>|exp)* <close-paren>
    vector = <left-square-bracket> (<ws>|exp)* <right-square-bracket>
    set = <number-sign> <left-curly-bracket> (<ws>|exp)* <right-curly-bracket>
    map = <left-curly-bracket> (<ws>|exp)* <right-curly-bracket>

    type-hint =  <circumflex> ('ints'|'floats'|'longs'|'objects'|symbol|keyword|map) <ws*>
    with-meta = (type-hint <ws+>)+ exp

    <literal> = character | symbol | string | number | nil | boolean | keyword | regex | with-meta | method
    <containers> = list | vector | set | map


     (***** forms *****)
     defn-form = <open-paren> <ws*> 
                   <'defn'>  <ws*> symbol <ws*> string* <ws*> map* <ws*> 
                      (fn-form-body|<ws>)* 
                 <close-paren>
     quote-form = <apostrophe> (literal | containers)
     deref-form = <commercial-at> exp
     syntax-quote-form = <syntax-quote> exp
     unquote-form = <unquote> exp
     unquote-splicing-form = <unquote-splicing> exp
     let-form = <open-paren> <ws*> <'let'> <ws*>  <left-square-bracket> <ws*> bindings <ws*> <right-square-bracket> <ws*> body  <ws*> <close-paren>
     var-quote-form = <number-sign> <apostrophe> exp
     def-form = <open-paren> <ws*> <'def'> <ws+> symbol (exp|<ws>)+ <close-paren>
     fn-form = <open-paren> <ws*> <'fn'> <ws*> (fn-form-body|<ws>)* <ws*> <close-paren>
     do-form = <open-paren> <ws*> <'do'> <ws+> body  <ws*> <close-paren>
     doto-form = <open-paren> <ws*> <'doto'> <ws+> (exp|<ws>)+  <ws*> <close-paren>
     if-form = <open-paren> <ws*> <'if'> <ws*> exp <ws*> exp <ws*> exp <close-paren>
     when-form = <open-paren> <ws*> <'when'> <ws*> exp (exp|<ws>)* <ws*><close-paren>
     lambda-form = <number-sign> <open-paren> <ws*> (exp|<ws>)* <close-paren>
     arrow-1-form = <open-paren> <ws*> <'->'> (exp|<ws>)* <close-paren>
     arrow-2-form = <open-paren> <ws*> <'->>'> (exp|<ws>)* <close-paren>

     <fn-form-body> = <open-paren*> <ws*> fn-args <ws*>  body  <ws*> <close-paren*>
     method = <'.'> #'[^\"\t\n\r,(): \\[\\]{}]+'


     (***** ns *****)

     ns-form = <open-paren> <ws*> <'ns'> <ws*> symbol <ws*> (ns-require|ns-import|ns-gen-class|<ws*>)* <ws*>  <close-paren>

     ns-require = <open-paren> <ws*> <':require'> (symbol|ns-vector|<ws*>)+ <close-paren>
     ns-import = <open-paren> <ws*> <':import'> (symbol|ns-vector|<ws*>)+ <close-paren>
     ns-vector = <left-square-bracket> <ws*> (symbol|keyword|<ws>)* <right-square-bracket>

     ns-gen-class = <open-paren> <ws*> <':gen-class'> <ws*> <close-paren>


     body = (exp|<ws>)*


     binding = <ws*> (destructuring-map|destructuring-vector|destructuring-symbol) <ws*> exp <ws*>

     <bindings> = binding*



     fn-args = <left-square-bracket> (destructuring|<ws>)* <right-square-bracket>


  
     (***** destructuring *****)

     <destructuring> =  destructuring-symbol|destructuring-map|destructuring-vector

     destructuring-symbol = symbol|with-meta
                           
     destructuring-map = <left-curly-bracket> (exp|destructuring-element-as|destructuring-element-or|<ws>)* <right-curly-bracket>
     destructuring-element-keys = <ws*> <':keys'> <ws*> vector <ws*>
     destructuring-element-as = <ws*> <':as'> <ws*> symbol <ws*>
     destructuring-element-or = <ws*> <':or'> <ws*> map <ws*>
     destructuring-vector = <left-square-bracket> (exp|<ws>)* <right-square-bracket>

")


;;(def parser (i/parser grammer))


(defparser parser grammer)


