(ns spiler.data-instaparse
  (:gen-class
   :name spiler.data.GeneratorInstaparse
   :methods [#^{:static true} [genFakeData [String, long] Object]
             #^{:static true} [parse [String] Object]
             #^{:static true} [db [String] Object]])
  (:require [silvur.util :refer [edn->json json->edn]]
            [silvur.datetime2 :refer [datetime date datetime* inst< -year adjust +time
                                      -month before after vec<  -hour -minute -second -day
                                      zone-offset UTC JST]]
            [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]
            [clojure.core.match :refer [match]]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [spiler.sql-instaparse :refer [ddl->edn]]))


(defonce first-names (with-open [rdr (io/reader (io/resource "first-name.csv"))]
                       (->> (rest (line-seq rdr))
                            (map #(str/split % #","))
                            (doall))))

(defonce given-names (with-open [rdr (io/reader (io/resource "given-name.csv"))]
                       (->> (rest (line-seq rdr))
                            (map #(str/split % #","))
                            (doall))))


(defmulti gen-random-data (fn [{:keys [type attribute constraint annotation]}]
                            (keyword (str/lower-case (or (:type annotation) type)))))



(defmethod gen-random-data :first-name [{:keys [character]}]
  (let [o (condp = (keyword character)
            :hiragana 0
            :roman 1
            :kanji 2
            1)]
    (str/capitalize (nth (nth first-names (rand-int (count first-names)) ) o))))

;; Given name
(defmethod gen-random-data :given-name [{:keys [character]}]
  (let [o (condp = (keyword character)
            :hiragana 0
            :roman 1
            :kanji 2
            1)]
    (str/capitalize (nth (nth given-names (rand-int (count given-names)) ) o))))

;; Human name
(defmethod gen-random-data :human-name [{:keys [character]}]
  (let [o (condp = (keyword character)
            :hiragana 0
            :roman 1
            :kanji 2
            1)
        first-name (gen-random-data {:type :first-name :character character})
        given-name (gen-random-data {:type :given-name :character character})]
    (condp = character
      :roman (str first-name " " given-name)
      (str given-name " " first-name))))


;; ;; Specific type
(defmethod gen-random-data :uuid [_]
  (str (random-uuid)))

(defmethod gen-random-data :enum [{name :name
                                   {:keys [collection]} :annotation}]
  (nth collection (int (* (count collection) (rand)))))



(defmethod gen-random-data :datetime [{:keys [attribute annotation table] }]
  (let [{:keys [from unit duration] :or {unit :hour duration 128}} annotation
        unit (keyword (str/lower-case (name unit)))]
    (+time (datetime (when from (mapv parse-long (str/split from #"[/\-:T]"))))
           ((condp = unit
              :second -second
              :minute -minute
              :hour -hour
              :day -day
              :month -month
              :year -year
              -hour)
            (long (rand-int duration))))))


(defmethod gen-random-data :date [args]
  (cond-> (gen-random-data (assoc-in args [:annotation :type] "datetime")) 
    :execute (.toLocalDate)
    (= type :varchar) str))


(defmethod gen-random-data :timestamp [{:keys [from-year to-year]
                                        :as args
                                        :or {from-year 2000}}]
  (-> (gen-random-data (assoc args :type :datetime))
      (+time (java.time.Duration/of (* 1000 (rand)) (java.time.temporal.ChronoUnit/MILLIS)))))


;; ;; General type
(defmethod gen-random-data :varchar [{:keys [attribute table] :as d}]
  (let [s (str "SAMPLE VC:" (:name d))]
    (subs s 0 (min (:length attribute) (count s)))))

(defmethod gen-random-data :decimal [{:keys [attribute annotation] }]
  (let [{at-p :precision at-s :scale :or {precision 4 scale 2}} attribute
        {an-p :precision an-s :scale :or {anp (int (/ precision 2))}} annotation
        precision (or an-p at-p)
        scale (or an-s at-s)]
    
    (+ (long (rand (reduce * 1 (repeat (- precision scale) 10)) ))
       (* (reduce * 1 (repeat scale 0.1))
          (rand-int (reduce * 1 (repeat scale 10)) )))))


(defmethod gen-random-data :boolean [_]
  (< 0.5 (rand)))

(defmethod gen-random-data :integer [{:keys [annotation]}]
  (let [{:keys [max min] :or {max Integer/MAX_VALUE min 0}} annotation]
    (int (+ min (* (- max min) (rand))))))

(defmethod gen-random-data :tinyint [{:keys [attribute]}]
  (let [{:keys [max min] :or {max 127 min -128}} attribute]
    (inc (+ min (* max (int (/ (rand-int 255) 255.0)))))))

(defmethod gen-random-data :smallint [{:keys [attribute]}]
  (let [{:keys [max min] :or {max 10000 min 0}} attribute]
    (int (+ min (* (- max min) (rand))))))

(defmethod gen-random-data :fn [{{:keys [f]} :annotation :as args}]
  (f args))

(defmethod gen-random-data :default [_]
  "DEFAULT SAMPLE")


(defn alter-table [db {elements :elements tbl :name}]
  ;; PRIMARY KEY: {:action "ADD", :definition "PRIMARY KEY", :columns ["ID"]}
  ;; ANNOTATION: {:name "ID", :definition "ANNOTATION", :action "ADD", :type "UUID"}

  (->> elements
       (reduce (fn [r {a :action d :definition t :type col :name :as e}]
                 (match [a d]
                        ["ADD" "COLUMN"] (update-in r [:tables (keyword tbl) :columns]
                                                    assoc (keyword col) e)
                        ["ADD" "PRIMARY KEY"] (update-in r [:tables (keyword tbl) :constrains] assoc :primary-key (:columns e))
                        ["ADD" "ANNOTATION"] (update-in r [:tables (keyword tbl) :columns (keyword col)] assoc :annotation (dissoc e :name :action :definition))))
               db)))

;; {:definition "FOREIGN KEY",
;;     :referencing ["TRANSACTION_ID"],
;;     :referenced {:name "FINANCIAL_TRANSACTION", :columns ["ID"]},
;;  :action "ADD"}

(defn create-table [db {tbl :name elements :elements :as table-def}]
  (->> elements
       (reduce (fn [r {a :action col :name d :definition :as e}]
                 (match [a d]
                        ["ADD" "COLUMN"] (assoc-in r [:tables (keyword tbl) :columns (keyword col)] e)
                        ["ADD" "FOREIGN KEY"] (update-in r [:tables (keyword tbl) :constraints :foreign-key] (comp vec conj) (select-keys e [:referencing :referenced]))))
               db)))

(defn gen-db 
  ([table-maps]
   (->> table-maps
        (reduce (fn [db {t :type n :name es :elements :as m}]
                  (condp = t
                    "CREATE TABLE" (create-table db m)
                    "ALTER TABLE" (alter-table db m)
                    :else db))
                {})))
  ([table-maps post-fn]
    (-> (gen-db table-maps)
        (post-fn))))


(defn gen-fake-data [db & [number-or-map]]
  (->> (:tables db)
       (reduce (fn [r0 [tbl {:keys [columns]}]]
                 (assoc r0 tbl (->> columns
                                    (reduce (fn [r1 [col m]]
                                              (assoc r1 col (gen-random-data m)))
                                            {})
                                    (fn [])
                                    (repeatedly)
                                    (take (if (map? number-or-map)
                                            (get number-or-map tbl 1)
                                            (or number-or-map 1)))
                                    (vec))))
               {})))


(defn -genFakeData [sql & [n]]
  (-> sql
      (ddl->edn)
      (gen-db)
      (gen-fake-data n)
      (edn->json)))

(defn -db [sql & [n]]
  (-> sql
      (ddl->edn)
      (gen-db)
      (edn->json)))

(defn -parse [sql & [n]]
  (-> sql
      (ddl->edn)
      (edn->json)))





;; (defn -dictionary []
;;   (with-open [rdr (io/reader (io/file "/home/myst/test-data.csv"))]
;;     (->> (rest (line-seq rdr))
;;          (map #(str/split % #","))
;;          (map (fn [[kn hn email & _]]
;;                 (let [[gvn fst] (str/split (str/replace email #"@.*" "") #"_")
;;                       [h-gvn h-fst ] (str/split hn #" ")
;;                       [k-gvn k-fst ] (str/split kn #" ")]
;;                   {:h-gvn h-gvn :h-fst h-fst :k-fst k-fst :k-gvn k-gvn :r-fst fst :r-gvn gvn})))
;;          (doall))))

