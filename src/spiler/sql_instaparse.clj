(ns spiler.sql-instaparse
  (:gen-class
   :name spiler.sql.SQLParser
   :methods [#^{:static true} [getSQLMeta [String] Object]]
   :constructors {[] []})
  (:require [instaparse.core :as i]
            [silvur.util :refer [edn->json json->edn]]
            [clojure.string :as str]
            [clojure.java.io :as io]
            ))

(def whitespace-or-comments
  (i/parser
   "ws-or-comments = #'[\\s;,]+' | comments
     comments = comment+
     comment = '/*' inside-comment+ '*/'
     inside-comment =  !( '*/' | '/*' ) #'.' | comment"
   :auto-whitespace :comma))


(def sql-92-parser (i/parser (slurp (io/file "resources/sql-92+mysql.bnf"))
                             :string-ci true
                             :auto-whitespace whitespace-or-comments
                             ))


(defn -init []
  [[] (ref {})])

(defn get-node [keys tree]
  (loop [ts tree
         ks keys
         r []]
    (cond
      (empty? ts) r
      (and (vector? (first ts))) (apply concat (get-node ks (first ts))
                                        (map #(get-node ks %) (vec (rest ts))))
      (and (vector? ts) (= (count ks) 1) (= (first ks) (first ts))) (cons (vec (rest ts)) r)
      (and (vector? ts) (> (count ks) 1) (= (first ks) (first ts))) (recur (vec (rest ts)) (rest ks) r)
      :else (recur (vec (rest ts)) ks r))))

(defn get-when [xs f]
  (first (filter f xs)))

(defn- transform [m & [debug?]]
  (if debug?
    m
    (i/transform {:table_element_list (fn [& xs]
                                        {:elements (->> (filter map? xs)
                                                        (sort-by :definition) ;; COLUMN -> FOREIGN KEY
                                                        (reduce conj []))})

                  ;;
                  :direct_SQL_data_statement (fn [x] x)
                  :direct_SQL_data_statements (fn [& xs] xs)
                  ;; TABLE
                  :table_name (fn [x]
                                {:name (get-in x [1 1 1 1 1 1])})
                  :table_element (fn [x] x)
                
                  :table_definition (fn [& xs]
                                      (let [table (->> (filter map? xs)
                                                       (reduce conj {:type "CREATE TABLE"}))]
                                        (update table :elements (fn [cols]
                                                                  (->> cols
                                                                       (filter map?)
                                                                       (mapv #(assoc % :action "ADD")))))))
                  ;; COLUMN
                  :column_definition (fn [& xs]
                                       (->> (filter map? xs)
                                            (reduce (fn [r x]
                                                      (merge-with (comp vec concat) r x))
                                                    {:definition "COLUMN"})))
                  ;; ON UPDATE
                  :on_update_clause (fn [& xs] {:on_update (last xs)})
                  ;; DATE/TIME
                  :datetime_type (fn [x] {:type x})
                  :current_timestamp_value_function (fn [x] x)
                  :datetime_value_function (fn [x] x)
                  ;; DECIMAL
                  :scale (fn [x] {:scale (parse-long x)})
                  :precision (fn [x] {:precision (parse-long x)})
                
                  :exact_numeric_type (fn [& xs]
                                        (let [attr (->> (filter map? xs)
                                                        (reduce conj {}))] 
                                          (cond-> {:type (first xs)}
                                            (seq attr) (assoc :attribute attr))))
                  :numeric_type (fn [x] x)
                  :period (fn [x] ".")
                  :unsigned_numeric_literal (fn [x] x)
                  :signed_numeric_literal (fn [x] x)
                  :literal (fn [x] x)
                  :default_option (fn [x] x)
                  :default_clause (fn [& xs] {:default (second xs)})
                  :exact_numeric_literal (fn [& xs] (let [z (str/join (map str xs))]
                                                      (if (.matches z ".*\\..*")
                                                        (parse-double z)
                                                        (parse-long z))))
                
                  ;; PRIMARY KEY
                  :unique_column_list (fn [x] x)
                  :unique_constraint_definition (fn [& xs]
                                                  (reduce conj
                                                          {:definition "PRIMARY KEY"}
                                                          (filter map? xs)))
                  ;; BOOLEAN
                  :boolean_type (fn [x] {:type x})
                
                  ;; VARCHAR
                  :unsigned_integer (fn [& xs]
                                      (str/join (map second xs))
                                      ;;(parse-long (str/join (map second xs)))
                                      )
                  :length (fn [& xs] {:length (first xs)})
                  :unique_specification (fn [& xs] (str/join " " xs))
                  :column_constraint (fn [& xs] {:constraint [(str/join " " xs)]})
                  :column_constraint_definition (fn [& xs] (first xs))
                  :character_string_type (fn [& xs]
                                           {:type (first xs)
                                            :attribute (->> (filter map? xs)
                                                            (reduce conj {}))})
                  :character_string_literal (fn [& xs]
                                              (first xs))
                  :column_name (fn [x] {:name (get-in x [1 1 1 1])})
                  :data_type (fn [& xs] (first xs))
                
                  :add_table_constraint_definition (fn [& xs]
                                                     ;; Expected only 1 for ALTER TABLE
                                                     ;; table_constraint_definition ::= [ constraint_name_definition ] table_constraint [ constraint_check_time ]
                                                     (->> (filterv map? xs)
                                                          (reduce conj)
                                                          (conj {:action "ADD"})))
                  :alter_table_statement (fn [& xs]
                                           (assoc (nth xs 2)
                                                  :type "ALTER TABLE"
                                                  :elements (->> (filterv map? (drop 3 xs)))))
                  :alter_table_action (fn [& xs]
                                        (first xs))
                
                  :add_column_definition (fn [& xs]
                                           (assoc (reduce conj {} (filter map? xs))
                                                  :action "ADD"))

                  ;; PRIMARY/FOREIGN KEY
                  :column_name_list (fn [& xs]
                                      {:columns (filterv identity (map :name xs))})
                  :reference_column_list (fn [& xs] (apply merge-with conj xs))
                  :referenced_table_and_columns (fn [& xs]
                                                  {:referenced (->> (filter map? xs)
                                                                    (reduce conj))})
                  :references_specification (fn [& xs]
                                              (->> (filter map? xs)
                                                   (reduce conj {})))
                  :referencing_columns (fn [& xs]
                                         {:referencing (vec (mapcat :columns xs))}
                                       
                                         )
                  :table_constraint (fn [x] x)
                  :referential_constraint_definition (fn [& xs]
                                                       (->> (filter map? xs)
                                                            (reduce conj {:definition "FOREIGN KEY"})))
                  :table_constraint_definition (fn [x] x)

                  ;; AUTO_INCREMENT (MySQL)
                  :auto_increment_constraint (fn [& xs]
                                               ;; AUTO_INCREMENT may not have an arg
                                               {:auto_increment (second xs)}
                                               ;;(str/trim (str "AUTO_INCREMENT " (second xs)))
                                               )


                  ;; Enhance
                  :alter_column_action (fn [& xs] (reduce conj {} (filter map? xs)))
                  :alter_column_definition (fn [& xs]
                                             (reduce conj {} (filterv map? xs))
                                             )
                  :set_annotation_clause (fn [& xs]
                                           (reduce conj {:definition "ANNOTATION"
                                                         :action "ADD"}
                                                   (filter map? xs)))
                  :annotation_clause (fn [& xs] (reduce conj {} (filter map? xs)))
                  :annotation_type_clause (fn [& xs] (first xs))
                  :annotation_type_enum_clause (fn [& xs] (reduce conj {:type "ENUM"} (filter map? xs)))
                  :annotation_enum_item_list (fn [& xs] {:collection (->> (filter map? xs)
                                                                          (mapv :name))})
                  :annotation_type_uuid_clause (fn [& xs] (reduce conj {:type "UUID"} (filter map? xs)))
                  :annotation_enum_item (fn [& xs]
                                          {:name (first xs)})
                  :annotation_type_decimal_clause (fn [& xs] (reduce conj {:type "DECIMAL"} (filter map? xs)))
                  :datetime_value (fn [x] x)
                  :date_string (fn [x] x)
                  :date_value (fn [& xs] (str/join (map second xs)))
                  :annotation_date_from (fn [& xs] {:from (last xs)})
                  :annotation_date_to (fn [& xs] {:to (last xs)})
                  :annotation_date_unit (fn [& xs] {:unit (last xs)})
                  :annotation_date_duration (fn [& xs] {:duration (parse-long (last xs))})            
                  :annotation_type_date_clause (fn [& xs] (reduce conj (filter map? xs)))}
               
                 m)))

(defn ddl->edn [sql-statement & {:keys [raw? trace?]}]
  (-> (sql-92-parser sql-statement :trace trace?)
      (transform raw?)
      vec))

(defn -getSQLMeta [sql]
  (-> sql ddl->edn edn->json))

