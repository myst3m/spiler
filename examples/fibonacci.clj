(ns fibonacci)

(def fib (fn [a b n c]
           (if (= n c)
             a
             (fib b (+ a b) n (inc c)))))

(if (> (count arg) 0)
  (println (fib ^int 0 1 (tonumber (first arg)) 0))
  (println "Usage: " "lua fibonacci.lua"))

