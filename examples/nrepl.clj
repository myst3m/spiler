;; Notes:
;; This script requires Lua fifo library.

(ns nrepl
  (:require [http.server :as http]
            [json :as json]
            [http.headers :as hdr]))


;; op=eval&code=%28%2B+1+2+3%29&id=def76e17-d3eb-4ffb-a475-bd4c6c5e77e4


(def gid nil)
(def sid "")

(def hex->char (fn [x]
                 (string/char (tonumber x 16))))

(defn decode
  "Decode to Character"
  ([url] (-> url
            (.gsub "(%+)" " ")
            (.gsub "%%(%x%x)" (fn [x]
                                (string/char (tonumber x 16)))))))

(def host "localhost")
(def port 12345)

(defn srv [host port]
  (http/listen  {:host host :port port
                 :onstream  (fn [server stream]
                              (let [rqh (.get_headers stream)
                                    rqm (.get rqh ":method")
                                    path (.get rqh ":path")
                                    msg (.get_body_as_string stream)
                                    rsh (hdr/new)
                                    id (string/gsub msg ".*id=([a-zA-Z0-9-]+)" "%1")
                                    code (string/gsub msg ".*&code=(.+)&.*" "%1")]

                                (println "MESSAGE:" msg)                                      
                                (println "CODE:" code)
                                


                                (when (= rqm "POST")
                                  (.append rsh "Set-Cookie" (str "drawbridge-session=" sid ";Path/;HttpOnly"))
                                  ;; Need to implement atom
                                  (set! gid id)
                                  (println "POST" gid))

 
                                (doto rsh
                                  (.append ":status" "200")
                                  (.append "Content-Type" "application/json"))

                                (let [result (eval (decode code))
                                      text (str "[" "\n"
                                                (json/encode {:id gid,
                                                              :session sid,
                                                              :value result
                                                              :ns "user"})
                                                "\n"
                                                (json/encode {:id gid,
                                                              :session sid,
                                                              :status ["done"]})
                                                "\n" "]")]
                                  (doto stream
                                    (.write_headers  rsh (= "HEAD" rqm) 1000)
                                    (.write_body_from_string text 1000)
                                    (.shutdown)))))}))

(when (first arg)
  (let [x (first arg)]
    (set! host x)))

(println (str "Boot Lua nREPL on " host ":" port))
(doto (srv host port) (.listen) (.loop))



;; (comment
;;   (def c (nrepl/client (dc/ring-client-transport "http://localhost:12345") 1000))
;;   (nrepl/message c {:op "eval" :code "(+ 1 2 3)"}))
