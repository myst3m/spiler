# spiler

A transpiler from Clojure to X

This is very early and limited software to transpile from Clojure.
This only supports Lua at this time.

## Install
```sh
$ git clone https://gitlab.com/myst3m/spiler
$ cd spiler
$ boot build
```

## Usage
Run spiler jar file with clojure file.

```sh
$ java -jar spiler-0.0.1-SNAPSHOT.jar -f examples/fibnacci.clj
Transpile:  examples/fibnacci.lua
$ lua examples/fibnacci.lua
12586269025
```

## Supported for Lua
 - Very limited built-in forms and functions/macros
   - arithmetic functions
   - comparators 
   - [] as Lua table
   - {} as Lua table (Hash)
   - str
   - vec   
   - map
   - let
   - def
   - defn
   - fn
   - do
   - if
   - when
   - ns
   - require
   - ->
   - ->>
   - doto
 - Spiler transpiles vector and list as a table , and also map.

## Todo
 - Implement more built-ins
 - Destructuring for arguments of functions
 - Refactoring BNF
 

 
